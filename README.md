## System variables to configure

* ```DIP_HOME = os.environ['DIP_HOME']```
* ```DKU_BIN = os.environ['DKUBIN']```
* ```DKU_PATH = os.environ['DKUPYTHONPATH']```
* ```DSN = os.environ['DKUDBINFO']```
