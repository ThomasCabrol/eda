from api import app
from settings import PORT, RUN_ON

app.run(debug = True, host = RUN_ON, port = PORT)
