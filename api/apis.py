import sys
from api import app
from settings import PORT, SERVER, DSN
from flask import jsonify
from helpers import *

# Using another module
from core import dku, stats, sql

# Endpoints
@app.route('/', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def api_home():
    payload = {
        'description': 'App Home',
        'endpoints': [
            {'url': '%s:%s/projects' % (SERVER, PORT), 'description':  'Dataiku Projects listing'},
            {'url': '%s:%s/sql/list-tables' % (SERVER, PORT), 'description':  'SQL tables list, per a specific ODBC DSN'},
        ]
    }
    return jsonify( payload )


@app.route('/projects', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def api_projects():

    projects = dku.projects.get_projects()

    endpoints = []
    for project in projects:
        endpoint = {}
        endpoint['project_name'] = project
        endpoint['description'] = 'List all datasets belonging to the project %s' % project
        endpoint['url'] = '%s:%s/%s/datasets' % (SERVER, PORT, project)
        endpoints.append( endpoint )

    payload = {
        'description': 'Dataiku Projects listing',
        'projects': projects,
        'endpoints': endpoints,
        'projects_count': len(projects)
    }

    return jsonify( payload )


@app.route('/<project>/datasets', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def api_datasets(project):

    datasets = dku.datasets.get_datasets(project)

    endpoints = []
    for dataset in datasets:
        endpoint = {}
        endpoint['project_name'] = project
        endpoint['dataset_name'] = dataset
        endpoint['description'] = 'Audit of dataset %s' % dataset
        endpoint['url'] = '%s:%s/%s/%s/dataset-audit' % (SERVER, PORT, project, dataset)
        endpoints.append( endpoint )
        endpoint = {}
        endpoint['project_name'] = project
        endpoint['dataset_name'] = dataset
        endpoint['description'] = 'Content of dataset %s' % dataset
        endpoint['url'] = '%s:%s/%s/%s/dataset-content' % (SERVER, PORT, project, dataset)
        endpoints.append( endpoint )

    payload = {
        'project_name': project,
        'datasets': datasets,
        'datasets_count': len(datasets),
        'description': 'List all datasets belonging to the project %s' % project,
        'endpoints': endpoints,
    }

    return jsonify( payload )


@app.route('/<project>/<dataset>/dataset-audit', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def api_dataset_audit(project, dataset):
    df = dku.dataset.get_dku_dataset('%s.%s' % (project, dataset))
    if df is not None:
        data = stats.audit.get_audit(df)
    else:
        data = None
    payload = {
        'project_name': project,
        'dataset_name': dataset,
        'description': 'Audit of dataset %s in project %s' % (dataset, project),
        'audit': data,
        'endpoints': []
    }
    return jsonify( payload )

@app.route('/<project>/<dataset>/dataset-content', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def api_dataset_content(project, dataset, limit=10):
    df = dku.dataset.get_dku_dataset('%s.%s' % (project, dataset), limit=limit)
    records = []
    if df is not None:
        cols = [str(col) for col in df.columns]
        for row in df.itertuples():
            data = [str(e) for e in row[1:]]
            record = dict(zip(cols, data))
            records.append(record)
    payload = {
        'project_name': project,
        'dataset_name': dataset,
        'description': 'Content of dataset %s in project %s' % (dataset, project),
        'records': records,
        'endpoints': []
    }
    return jsonify( payload )


@app.route('/sql/list-tables', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def api_sql_list_tables():

    tables = sql.vertica_audit.list_tables(DSN)

    endpoints = []
    for table in tables:
        endpoint = {}
        endpoint['dsn'] = DSN
        endpoint['table_name'] = table
        endpoint['description'] = 'List of columns in table %s' % table
        endpoint['url'] = '%s:%s/sql/%s/columns' % (SERVER, PORT, table)
        endpoints.append( endpoint )

    payload = {
        'description': 'List of tables available in the connection %s' % DSN,
        'dsn': DSN,
        'tables': sorted(tables),
        'tables_count': len(tables),
        'endpoints': endpoints
    }

    return jsonify( payload )


@app.route('/sql/<table>/columns', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def api_sql_list_columns(table):

    columns = sql.vertica_audit.list_columns(DSN, table)

    endpoints = []
    for column in columns:
        endpoint = {}
        endpoint['dsn'] = DSN
        endpoint['table_name'] = table
        endpoint['column_name'] = column
        endpoint['description'] = 'Analysis of column %s in table %s' % (column, table)
        endpoint['url'] = '%s:%s/sql/%s/analyze/%s' % (SERVER, PORT, table, column)
        endpoints.append( endpoint )

    payload = {
        'description': 'List of columns available in table %s' % table,
        'dsn': DSN,
        'table': table,
        'columns': columns,
        'columns_count': len(columns),
        'endpoints': endpoints
    }

    return jsonify( payload )


@app.route('/sql/<table>/analyze/<column>', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def api_sql_audit_column(table, column):

    audit = sql.vertica_audit.audit_column(DSN, table, column)

    endpoints = []

    payload = {
        'description': 'Analysis of column %s in table %s' % (column, table),
        'dsn': DSN,
        'table': table,
        'column': column,
        'audit': audit,
        'endpoints': endpoints
    }

    return jsonify( payload )