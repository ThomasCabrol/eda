from flask import Flask

# Flask application
app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = '123454321'

import api.apis