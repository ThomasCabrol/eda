import os
import sys

# Global API settings
SERVER = 'http://localhost'
PORT = 9899
RUN_ON = '0.0.0.0'

# Dataiku DSS interaction
DIP_HOME = os.environ['DIP_HOME']
DKU_BIN = os.environ['DKUBIN']
DKU_PATH = os.environ['DKUPYTHONPATH']

# DB interaction
DSN = os.environ['DKUDBINFO']

#DSN = 'DSN=vpg'

#DIP_HOME = '/Users/thomas/Documents/dataiku/dss-data-dir/1.1.3'
#DKU_BIN = '/Users/thomas/Documents/dataiku/dip/dev/dku'
#DKU_PATH = '/Users/thomas/Documents/dataiku/dip/src/main/python'
#os.environ['DIP_HOME'] = DIP_HOME
#os.environ['DKUBIN'] = DKU_BIN

sys.path.append(DKU_PATH)

# Stats settings
MIGHT_BE_KEY = 0.99

