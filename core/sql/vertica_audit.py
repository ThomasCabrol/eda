import pyodbc
from settings import DSN

def list_tables(connection):
    conn = pyodbc.connect(connection)
    q = "SELECT DISTINCT table_name FROM columns WHERE table_schema = 'public'"
    tables = conn.execute(q).fetchall()
    conn.close()
    return [table[0] for table in tables]

def list_columns(connection, table):
    conn = pyodbc.connect(connection)
    q = "SELECT column_name FROM columns WHERE table_schema = 'public' AND table_name = '%s'" % str(table)
    columns = conn.execute(q).fetchall()
    conn.close()
    return [column[0] for column in columns]

def audit_column(connection, table, column):
    conn = pyodbc.connect(connection)
    audit = {}

    q = 'SELECT COUNT(*) FROM "%s"' % str(table)
    audit['records_count'] = conn.execute(q).fetchone()[0]

    q = 'SELECT COUNT(DISTINCT "%s") FROM %s' % (str(column), str(table))
    audit['cardinality'] = conn.execute(q).fetchone()[0]
    audit['is_key'] = audit['records_count'] == audit['cardinality']

    q = 'SELECT COUNT(*) FROM "%s" WHERE "%s" IS NULL' % (str(table), str(column))
    audit['missing_values'] = conn.execute(q).fetchone()[0]

    q = 'SELECT MIN("%s") FROM "%s"' % (str(column), str(table))
    audit['min_value'] = conn.execute(q).fetchone()[0]

    q = 'SELECT MAX("%s") FROM "%s"' % (str(column), str(table))
    audit['max_value'] = conn.execute(q).fetchone()[0]

    q = 'SELECT "%s" AS value, COUNT(*) AS records FROM "%s" GROUP BY 1 ORDER BY 2 DESC LIMIT 10' % (str(column), str(table))
    audit['d3_block'] = []
    audit['d3_data'] = {}
    audit['d3_data']['key'] = 'Top 10 Values'
    audit['d3_data']['color'] = '#4f99b4'
    audit['d3_data']['values'] = []
    for r in conn.execute(q).fetchall():
        top_value = {}
        top_value['label'] = r[0]
        top_value['value'] = r[1]
        audit['d3_data']['values'].append(top_value)
    audit['d3_block'].append(audit['d3_data'])

    return audit


if __name__ == '__main__':
    r = audit_column(DSN, 'emv_bounced_history_2014_t', 'EMAIL')
    print r