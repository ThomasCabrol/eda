import numpy as np
from unidecode import unidecode
from settings import MIGHT_BE_KEY

def slugify(chain):
    try:
        _c = chain.decode('utf-8')
    except:
        _c = str(chain).decode('utf-8')
    allowed = " 0123456789abcdefghijklmnopqrstuvwxyz_"
    c = unidecode(_c.strip().lower())
    return ''.join([letter for letter in c if letter in allowed])

def get_audit(dataframe):
    stats = {}
    # Dimension statistics
    stats['rows_count'] = dataframe.shape[0]
    stats['columns_count'] = dataframe.shape[1]
    # Per column statistics
    stats['columns'] = []
    for i, col in enumerate(dataframe):
        info = {}
        info['_position'] = i + 1
        info['name_source'] = col
        info['name_slug'] = slugify(col)
        info['cardinality'] = dataframe[col].nunique()
        info['missing_values_count'] = dataframe[col].isnull().sum()
        info['missing_values_pct'] = np.float64(dataframe[col].isnull().sum()) / dataframe.shape[0] \
            if dataframe.shape[0] > 0 else 0
        dt = str(dataframe[col].dtype)
        info['data_type_pandas'] = dt
        if 'float' in dt.lower() or 'int' in dt.lower():
            info['data_type_macro'] = 'Numeric'
        else:
            info['data_type_macro'] = 'Category'
        info['is_dataset_key'] = False
        info['might_be_dataset_key'] = False
        if 'object' in dt.lower() or 'int' in dt.lower():
            if np.int64(info['cardinality']) == np.int64(dataframe.shape[0]):
                info['is_dataset_key'] = True
            if np.float64(info['cardinality']) / np.int64(dataframe.shape[0]) >= MIGHT_BE_KEY:
                info['might_be_dataset_key'] = True
        # Sample values
        info['sample_values'] = ["%s..." % str(e)[0:30] if len(str(e)) > 30 else str(e) for e in dataframe[col].unique()[0:5]]
        stats['columns'].append(info)
    # Data type statistics
    stats['numeric_columns'] = []
    stats['category_columns'] = []
    stats['integer_columns'] = []
    stats['float_columns'] = []
    for stat in stats['columns']:
        if stat['data_type_macro'] == 'Numeric':
            stats['numeric_columns'].append( stat['name_source'] )
            if 'float' in stat['data_type_pandas']:
                stats['float_columns'].append( stat['name_source'] )
            else:
                stats['integer_columns'].append( stat['name_source'] )
        else:
            stats['category_columns'].append( stat['name_source'] )
    stats['numeric_columns_count'] = len(stats['numeric_columns'])
    stats['category_columns_count'] = len(stats['category_columns'])
    stats['float_columns_count'] = len(stats['float_columns'])
    stats['integer_columns_count'] = len(stats['integer_columns'])
    return stats
