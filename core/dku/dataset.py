import dataiku

def get_dku_dataset(dataset, limit=None):
    try:
        return dataiku.Dataset(dataset).get_dataframe(limit=limit)
    except Exception, e:
        print str(e)
        return None