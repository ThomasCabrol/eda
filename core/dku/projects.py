from settings import DIP_HOME
import os

def get_projects():
    path = os.path.join(DIP_HOME, 'config', 'projects')
    return os.listdir(path)

