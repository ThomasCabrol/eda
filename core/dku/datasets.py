from settings import DIP_HOME
import os

def get_datasets(project):
    path = os.path.join(DIP_HOME, 'config', 'projects', project, 'datasets')
    if os.path.isdir(path):
        return [n.replace('.json', '') for n in os.listdir(path)]
    else:
        return []
